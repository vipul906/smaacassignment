# Hiring @ [smaac.in](https://smaac.in/)

- Thanks for showing interest in our job posting. You can read more about the company at [smaac.in](https://smaac.in/).
- Your access to this repo will expire in 5 days. Please clone the repo before that.
- The time limit for this hiring task is 5 days. Submissions after the limit will not be considered.

## Todo

- Clone this repo.
- Create your own public repo on gitlab/github with this code.
- Work in a separate branch. We will need to create a merge request later on for the evaluation criteria.
- Create a new table in the database called `Inventory`. It should contain item name and quantity for fields.
- Create a form in Vue to add new items into that table.
- Allow editing the name/quantity of any entry in that table.
- Allow deleting values in the table.

## Evaluation criteria

1. Record a [loom video](https://www.loom.com/) for example to show us how it works.
2. Create a merge request in your repo so that review is possible for the work you have done.
3. Code review of merge request over video call. Max 45 minutes.

## Code layout

- API code lives in `api` folder. This is a python flask project.
    - `requirements.txt` should allow you to install dependencies.
    - `python -m api.run` will then run the api on port 5000.
- Website project lives in `site` folder. This is a vue JS project.
    - `yarn.lock` should allow you to install dependencies.
    - `yarn serve` should allow you to run the UI on port 8080
- There is an existing module called `Sales` for your reference. It lives in `api/api/modules.py`.
- In the UI you can call any api using `/api/<slug>/<op>` pattern
- These api calls are routed to `modules.<class with slug>.handle_get_<op>` functions.
- Take a look at `modules.Sales` class to see how the API call to `/api/sales/list` is being handled.

from api.db import exec_to_json
from api.mixins import register, BaseMixin


@register
class Sales(BaseMixin):
    # Base
    slug = "sales"

    @classmethod
    def handle_get_list(cls, *, kwargs):
        return exec_to_json("select 1 as c1, 2 as c2")

import pprint
from flask_sqlalchemy import SQLAlchemy
from flask import current_app
from sqlalchemy import text


db = SQLAlchemy()


def exec_to_json(sql, *, returns_rows=True, **kwargs):
    """
    Execute and return response as JSON
    {
        "result": [
            {
                "colname": "val",
                "colname": "val",
                ...
            }

        ]
    }
    """
    results = db.session.execute(
        text(sql),
        kwargs,
    )
    res = {}
    if returns_rows:
        res = {
            "result": [{col: val for col, val in dict(row).items()} for row in results]
        }
    db.session.commit()
    return res

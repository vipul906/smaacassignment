from sqlalchemy import text
from flask import request

from api.db import db


class AuditLog(db.Model):
    __tablename__ = "AuditLog"
    # Fields
    id = db.Column(db.Integer, primary_key=True)
    msg = db.Column(db.String)
    created = db.Column(db.DateTime)

    @staticmethod
    def add(msg):
        addr = request.headers.getlist("X-Forwarded-For")
        addr = addr[0] if addr else ""
        msg = f"{msg}:{addr} {request.user_agent.platform} {request.user_agent.browser}"
        db.session.add(AuditLog(msg=msg))
        db.session.commit()

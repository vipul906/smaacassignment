from flask import abort

from api.db import db, exec_to_json
from api import modules, mixins  # pylint: disable=unused-import


def handle_slug_view(method, slug, op, *, kwargs):
    if slug not in mixins.klassMap:
        raise abort(404)
    klass = mixins.klassMap[slug]
    handler = f"handle_{method.lower()}_{op}"
    if not hasattr(klass, handler):
        raise abort(404)
    handler = getattr(klass, handler)
    result = handler(kwargs=kwargs)
    return {"ok": True, **result}
